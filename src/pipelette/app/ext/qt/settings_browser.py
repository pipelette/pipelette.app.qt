import logging
import asyncio

import qasync
import aiohttp

from fastapi import APIRouter

from qtpy import QtWidgets, QtGui

from pipelette.app.extensions.extension import Extension
from pipelette.app.settings.models import BaseSettingsModel


logger = logging.getLogger(__name__)


class SettingsUI(QtWidgets.QWidget):
    def __init__(self, parent, app):
        super(SettingsUI, self).__init__(parent)
        self.setWindowTitle("Pipelette - Settings Browser")
        self._app = app

        from qt_jsonschema_form import WidgetBuilder

        self._form_builder = WidgetBuilder()

        self._build()

    def _build(self):
        main_lo = QtWidgets.QVBoxLayout(self)

        h_lo = QtWidgets.QHBoxLayout(self)
        main_lo.addLayout(h_lo)

        self._list = QtWidgets.QListWidget(self)
        self._list.addItems(sorted(self._app.extension_names()))
        self._list.currentTextChanged.connect(self._on_extension_name_changed)
        h_lo.addWidget(self._list)

        scroll = QtWidgets.QScrollArea(self)
        scroll.setWidgetResizable(
            True
        )  # <- that fucking one took an hour of my life ! >:(
        self._form_host = QtWidgets.QWidget(scroll)
        self._form_host.setLayout(QtWidgets.QVBoxLayout())
        self._form_host.layout().setContentsMargins(0, 0, 0, 0)

        scroll.setWidget(self._form_host)
        h_lo.addWidget(scroll)

    def _on_extension_name_changed(self, extension_name):
        self.clear_settings_forms()
        self.add_settings_form(extension_name)

    def clear_settings_forms(self):
        layout = self._form_host.layout()
        while layout.count():
            child = layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()

    def add_settings_form(self, extension_name):
        settings = self._app.get_settings(extension_name)
        ui_schema = None
        form = self._form_builder.create_form(settings.schema(), ui_schema={})
        form.widget.state = settings.dict()
        form.widget.on_changed.connect(lambda d: print(d))
        self._form_host.layout().addWidget(form)


class SettingsBrowser(Extension):
    @classmethod
    def extension_package_name(cls):
        return "pipelette.app.ext.qt"

    @classmethod
    def extension_module_name(cls):
        return "pipelette.app.ext.qt.plugin"

    def init(self):
        self._top_level = None

    def install(self):
        pass

    def uninstall(self):
        if self._top_level is not None:
            self._top_level.deleteLater()
        self._top_level = None

    def _on_app_ready(self):
        self._app.get_extension("TrayIcon").add_tray_action(
            "Config/Settings Browser", self.show_ui
        )

    def _on_app_run(self):
        self.show_ui()

    def show_ui(self):
        if self._top_level is None:
            self._build_ui()
        self._top_level.show()

    def _build_ui(self):
        self._top_level = SettingsUI(None, self._app)
