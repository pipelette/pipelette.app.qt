import logging

from qtpy import QtWidgets, QtGui
import qasync

from pipelette.app.extensions.extension import Extension
from pipelette.app.settings.models import BaseSettingsModel


logger = logging.getLogger(__name__)

# The qasync logger is chatty, let's make is quietter:
logging.getLogger("qasync").setLevel(logging.INFO)


class QtAsyncApp(Extension):
    @classmethod
    def extension_package_name(cls):
        return "pipelette.app.ext.qt"

    @classmethod
    def extension_module_name(cls):
        return "pipelette.app.ext.qt.plugin"

    def init(self):
        self._loop = None
        self._qapp = None
        self._on_app_ready_callbacks = []

    def install(self):
        pass

    def uninstall(self):
        pass

    def _on_app_ready(self):
        qapp = QtWidgets.QApplication.instance()
        if qapp is None:
            qapp = QtWidgets.QApplication([])
            qapp.setQuitOnLastWindowClosed(False)

        self._qapp = qapp

        self._loop = qasync.QEventLoop(qapp)
        self._app.get_extension("AsyncLoop").set_event_loop(self._loop)
        logger.info("Patched Event Loop for Qt: %r", self._loop)

        logger.info("Calling app ready callbacks:")
        for callback in self._on_app_ready_callbacks:
            logger.info("  %s", callback)
            callback(self._qapp)

    def _on_app_closing(self):
        return False  # dont prevent...

    def _on_app_closed(self):
        self._qapp.closeAllWindows()
        # FIXME: on event loop stop: `QThread: Destroyed while thread is still running`
        # This didn't fix it:
        # self._loop.stop()
        # self._loop.close()

    #
    # Extension commands
    #
    def add_on_app_ready(self, callback):
        """
        The given callback will be called with the qt application
        as arguments as soon as it is running.
        """
        self._on_app_ready_callbacks.append(callback)
        if self._qapp is not None:
            callback(self._qapp)
