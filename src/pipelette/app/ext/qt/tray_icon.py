import logging

from qtpy import QtWidgets, QtGui

from pipelette.app.extensions.extension import Extension
from pipelette.app.settings.models import BaseSettingsModel


logger = logging.getLogger(__name__)


class TrayIcon(Extension):
    @classmethod
    def extension_package_name(cls):
        return "pipelette.app.ext.qt"

    @classmethod
    def extension_module_name(cls):
        return "pipelette.app.ext.qt.plugin"

    def init(self):
        self._tray_icon = None
        self._tray_actions = []
        self._tray_menu = None

    def install(self):
        pass

    def uninstall(self):
        pass

    def _on_app_ready(self):
        self._app.get_extension("QtAsyncApp").add_on_app_ready(self._build_tray_icon)

    def _build_tray_icon(self, qapp):
        if not QtWidgets.QSystemTrayIcon.isSystemTrayAvailable():
            raise Exception("No system tray support... goodbye then :/")

        icon = qapp.style().standardIcon(QtWidgets.QStyle.SP_DesktopIcon)
        if icon.isNull():
            raise Exception("No icon for tray, you wouldnt be able to quit ! :/")

        self._tray_icon = QtWidgets.QSystemTrayIcon(icon, qapp)
        self._tray_icon.setToolTip("Pipelette")

        def on_activated(reason):
            print("!!!", reason, "TODO: handle this.")
            for name, callback in self._tray_actions:
                print(name, ":", callback)

        self._tray_icon.activated.connect(on_activated)
        menu = self._get_tray_menu()
        self._tray_icon.setContextMenu(menu)
        self._tray_icon.show()

        self._app.set_notifier(self._notifier)
        self._app.notify(
            "Pipelette",
            "Hello you beautiful human !\n"
            "Pipelette is now available in your tray icons :)\n"
            "Have fun with it !",
        )

    def _notifier(self, title, message):
        self._tray_icon.showMessage(title, message)

    def _get_tray_menu(self):
        """
        This must not be called before qapp exists !
        """
        if self._tray_menu is None:
            self._tray_menu = QtWidgets.QMenu()
        self._update_tray_menu()
        return self._tray_menu

    def _update_tray_menu(self):
        if self._tray_menu is None:
            # not ready yet, it will be updated after qapp creation
            return

        submenus = {}
        self._tray_menu.clear()
        for name, callback in self._tray_actions:
            # TODO: support nested sub-menus here
            names = name.rsplit("/", 1)
            try:
                submenu_name, name = names
            except ValueError:
                menu = self._tray_menu
            else:
                try:
                    menu = submenus[submenu_name]
                except:
                    sub_menu = QtWidgets.QMenu(submenu_name, self._tray_menu)
                    submenus[submenu_name] = sub_menu
                    self._tray_menu.addMenu(sub_menu)
                    menu = sub_menu
            action = menu.addAction(name)
            print("+++++++++++++", name)
            action.triggered.connect(callback)

        self._tray_menu.addSeparator()
        exitAction = self._tray_menu.addAction("Exit")
        exitAction.triggered.connect(self._app.close)

    #
    # Extension commands
    #
    def add_tray_action(self, name, callback):
        """
        Adds an entry in the tray menu.

        Use `/` in name to create a sub-menu.
        """
        self._tray_actions.append((name, callback))
        self._update_tray_menu()
