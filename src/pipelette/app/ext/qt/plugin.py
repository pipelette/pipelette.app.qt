from pipelette.app.extensions.extension import app_extension, Extension

from .qt_async_app import QtAsyncApp
from .tray_icon import TrayIcon
from .extension_browser import ExtensionBrowser
from .settings_browser import SettingsBrowser


@app_extension
def add_extension(app):
    return QtAsyncApp


@app_extension(specname="add_extension")
def add_tray_icon_extension(app):
    return TrayIcon


@app_extension(specname="add_extension")
def add_extension_browser_extension(app):
    return ExtensionBrowser


@app_extension(specname="add_extension")
def add_settings_browser_extension(app):
    return SettingsBrowser
