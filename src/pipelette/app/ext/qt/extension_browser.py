import logging
import asyncio

import qasync
import aiohttp

from fastapi import APIRouter

from qtpy import QtWidgets, QtGui

from pipelette.app.extensions.extension import Extension
from pipelette.app.settings.models import BaseSettingsModel


logger = logging.getLogger(__name__)

extension_browser_router = APIRouter(tags=["Extensions"])


@extension_browser_router.get("/show_browser")
async def show_extensions_browser():
    return ExtensionBrowser.get().show_ui()


class Browser(QtWidgets.QWidget):
    def __init__(self, parent, app):
        super(Browser, self).__init__(parent)
        self._app = app
        self._app_client = app.get_extension("RESTApp").get_app_client()

        self.setWindowTitle("Pipelette App Extensin Browser")

        self.setLayout(QtWidgets.QVBoxLayout())

        l = QtWidgets.QLabel(self)
        l.setText("Click Refresh to see extension info")
        self.layout().addWidget(l)

        self._te = QtWidgets.QTextEdit(self)
        self.layout().addWidget(self._te)

        b = QtWidgets.QPushButton("Refresh", self)
        b.clicked.connect(self._on_refresh_button)
        self.layout().addWidget(b)

    @qasync.asyncSlot()
    async def _on_refresh_button(self):
        print("refresh...")
        infos = await self._app_client.get("/extensions/installed")
        self._te.clear()
        for info in infos:
            self._te.append(
                (
                    "#--- {name}\n"
                    "  package: {package}\n"
                    "  version: {version}\n"
                    "  module: {module}\n"
                ).format(**info)
            )


class ExtensionBrowser(Extension):
    @classmethod
    def extension_package_name(cls):
        return "pipelette.app.ext.qt"

    @classmethod
    def extension_module_name(cls):
        return "pipelette.app.ext.qt.plugin"

    def init(self):
        self._top_level = None

    def install(self):
        pass

    def uninstall(self):
        if self._top_level is not None:
            self._top_level.deleteLater()
        self._top_level = None

    def _on_app_ready(self):
        self._app.get_extension("RESTApp").include_router(
            extension_browser_router, "/extensions"
        )
        self._app.get_extension("TrayIcon").add_tray_action(
            "Config/Extension Browser", self.show_ui
        )

    # ----------

    def _build_ui(self):
        self._top_level = Browser(None, self._app)

    #
    # Extenstion commands
    #

    def show_ui(self, *args, **kwargs):
        if self._top_level is None:
            self._build_ui()
        self._top_level.show()
