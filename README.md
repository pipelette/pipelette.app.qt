# pipelette.app.ext.qt

`pipelette.app` extension to support Qt gui.

## QtApp

The `QtAsyncApp` extension integrates an async Qt event loop to the asyncio loop managed by the `AsyncLoop` extension.

## TrayIcon

The `TrayIcon` extension adds a tray icon with a menu that other extension can populate.
